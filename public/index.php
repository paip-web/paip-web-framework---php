<?php
// Load Config
require_once "../app/config/config.php";
// PHP Version Checker
require_once "../app/libraries/PHP.php";
PHP::dieByVersion("5.6.0");
// Require Bootstraping Script
require_once "../app/bootstrap.php";

// Init Core Library
$init = new Core();