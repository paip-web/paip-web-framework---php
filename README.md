# PAiP Web Framework
This is repository of PAiP Web Framework for PHP

## What is PAiP Web Framework ?
PAiP Web Framework (or PWF in short) is framework to make developing Web Apps and Websites simpler in PHP simpler.

## Actual Features of PWF

* `API Controller` - This is controller with set of functions to make developing API easier.
* `Controller` - This is just basic controller with set of function to make developing logic easier.
* `Database` - This is database manager class to make things easier.