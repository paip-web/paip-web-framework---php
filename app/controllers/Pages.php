<?php
/**
 * Pages Controller
 * @author Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

/**
 * Pages Controller
 */
class Pages extends Controller
{
    /**
     * Pages constructor.
     */
    public function __construct()
    {
    }

    /**
     * Main Route
     */
    public function index()
    {
        $data = ["title" => PWF\PWF::__FULL_NAME__];
        $this->view("pages/index", $data);
    }
}