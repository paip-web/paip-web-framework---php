<?php
/**
 * API Class
 * @author Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

/**
 * API Class
 */
class APIClass
{
    /**
     * Tools Class
     */
    /**
     * Is set ?
     * @param array $var Variable
     * @param string $attr Atribute
     * @param bool $nonull Is it can't be null if yes then true
     * @return bool|null True on Existance, False on not Existance, Null on magic
     */
    public static function T__IS($var, $attr, $nonull=false)
    {
        if($nonull)
        {
            if(isset($var[$attr]) && $var[$attr] !== null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if(isset($var[$attr]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return null;
    }
    /**
     * Get value if exist
     */
    public static function T__get($var, $attr)
    {
        if(APIClass::T__IS($var, $attr))
        {
            return $var[$attr];
        }
        return null;
    }
    public static function T__notin($source, $not, $key=false)
    {
        $ret = [];
        if($key)
        {
            foreach($source as $k => $v)
            {
                if(!isset($not[$k]))
                {
                    $ret[$k] = $v;
                }
            }
        }
        else
        {
            foreach($source as $v)
            {
                if(!in_array($v, $not))
                {
                    $ret[] = $v;
                }
            }
        }
        return $ret;
    }
    /**
     * API CLASS CONTENT
     */
    const __http_codes = [
        100,101,200,201,202,203,204,205,206,300,301,302,303,304,307,308,400,401,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,422,425,426,428,429,431,451,500,501,502,503,504,505,511];
    /**
     * Weird HTTP Status Codes that need another header to be as status code.
     */
    public static function __sc_http_codes()
    {
        return array_merge(
            [418],
            APIClass::T__notin(
                range(100, 999, 1),
                self::__http_codes
            )
        );
    }
    /**
     * Respond as API with JSON
     * @param mixed $data Data to respond with.
     */
    public static function JSONResponse($data, $http=array())
    {
        $http["content-type"] = "application/json";
        return APIClass::Response($http, json_encode($data));
    }
    /**
     * Get HTTP Status Array
     * @param int $code Code to Process
     * @return array HTTP Status Code Array
     */
    public static function getHTTPStatus($code)
    {
        $ret = array();
        if(is_string($code)) { $code = intval($code); }
        $ret = ($code == 100) ? ["code" => 100, "msg" => "Continue"] : $ret;
        $ret = ($code == 101) ? ["code" => 101, "msg" => "Switching Protocols"] : $ret;
        $ret = ($code == 200) ? ["code" => 200, "msg" => "OK"] : $ret;
        $ret = ($code == 201) ? ["code" => 201, "msg" => "Created"] : $ret;
        $ret = ($code == 202) ? ["code" => 202, "msg" => "Accepted"] : $ret;
        $ret = ($code == 203) ? ["code" => 203, "msg" => "Non-Authoritative Information"] : $ret;
        $ret = ($code == 204) ? ["code" => 204, "msg" => "No Content"] : $ret;
        $ret = ($code == 205) ? ["code" => 205, "msg" => "Reset Content"] : $ret;
        $ret = ($code == 206) ? ["code" => 206, "msg" => "Partial Content"] : $ret;
        $ret = ($code == 300) ? ["code" => 300, "msg" => "Multiple Choices"] : $ret;
        $ret = ($code == 301) ? ["code" => 301, "msg" => "Moved Permanently"] : $ret;
        $ret = ($code == 302) ? ["code" => 302, "msg" => "Found"] : $ret;
        $ret = ($code == 303) ? ["code" => 303, "msg" => "See Other"] : $ret;
        $ret = ($code == 304) ? ["code" => 304, "msg" => "Not Modified"] : $ret;
        $ret = ($code == 307) ? ["code" => 307, "msg" => "Temporary Redirect"] : $ret;
        $ret = ($code == 308) ? ["code" => 308, "msg" => "Permanent Redirect"] : $ret;
        $ret = ($code == 400) ? ["code" => 400, "msg" => "Bad Request"] : $ret;
        $ret = ($code == 401) ? ["code" => 401, "msg" => "Unauthorized"] : $ret;
        $ret = ($code == 403) ? ["code" => 403, "msg" => "Forbidden"] : $ret;
        $ret = ($code == 404) ? ["code" => 404, "msg" => "Not Found"] : $ret;
        $ret = ($code == 405) ? ["code" => 405, "msg" => "Method Not Allowed"] : $ret;
        $ret = ($code == 406) ? ["code" => 406, "msg" => "Not Acceptable"] : $ret;
        $ret = ($code == 407) ? ["code" => 407, "msg" => "Proxy Authentication Required"] : $ret;
        $ret = ($code == 408) ? ["code" => 408, "msg" => "Request Timeout"] : $ret;
        $ret = ($code == 409) ? ["code" => 409, "msg" => "Conflict"] : $ret;
        $ret = ($code == 410) ? ["code" => 410, "msg" => "Gone"] : $ret;
        $ret = ($code == 411) ? ["code" => 411, "msg" => "Length Required"] : $ret;
        $ret = ($code == 412) ? ["code" => 412, "msg" => "Precondition Failed"] : $ret;
        $ret = ($code == 413) ? ["code" => 413, "msg" => "Payload Too Large"] : $ret;
        $ret = ($code == 414) ? ["code" => 414, "msg" => "URI Too Long"] : $ret;
        $ret = ($code == 415) ? ["code" => 415, "msg" => "Unsupported Media Type"] : $ret;
        $ret = ($code == 416) ? ["code" => 416, "msg" => "Range Not Satisfiable"] : $ret;
        $ret = ($code == 417) ? ["code" => 417, "msg" => "Expectation Failed"] : $ret;
        $ret = ($code == 418) ? ["code" => 418, "msg" => "I'm a teapot"] : $ret;
        $ret = ($code == 422) ? ["code" => 422, "msg" => "Unprocessable"] : $ret;
        $ret = ($code == 425) ? ["code" => 425, "msg" => "Too Early"] : $ret;
        $ret = ($code == 426) ? ["code" => 426, "msg" => "Upgrade Required"] : $ret;
        $ret = ($code == 428) ? ["code" => 428, "msg" => "Precondition Required"] : $ret;
        $ret = ($code == 429) ? ["code" => 429, "msg" => "Too Many Requests"] : $ret;
        $ret = ($code == 431) ? ["code" => 431, "msg" => "Request Header Fields Too Large"] : $ret;
        $ret = ($code == 451) ? ["code" => 451, "msg" => "Unavailable For Legal Reasons"] : $ret;
        $ret = ($code == 500) ? ["code" => 500, "msg" => "Internal Server Error"] : $ret;
        $ret = ($code == 501) ? ["code" => 501, "msg" => "Not Implemented"] : $ret;
        $ret = ($code == 502) ? ["code" => 502, "msg" => "Bad Gateway"] : $ret;
        $ret = ($code == 503) ? ["code" => 503, "msg" => "Service Unavailable"] : $ret;
        $ret = ($code == 504) ? ["code" => 504, "msg" => "Gateway Timeout"] : $ret;
        $ret = ($code == 505) ? ["code" => 505, "msg" => "HTTP Version Not Supported"] : $ret;
        $ret = ($code == 511) ? ["code" => 511, "msg" => "Network Authentication Required"] : $ret;
        if(is_array($code) && APIClass::T__IS($code, "code") && APIClass::T__IS($code, "msg"))
        {
            $ret = $code;
        }
        return $ret;
    }
    /**
     * HTTP Response
     * @param array $http HTTP Headers
     * @param mixed $data Data to Respond with
     * @return bool True after response
     */
    public static function Response($http, $data)
    {
        if(APIClass::T__IS($http, "content-type"))
        {
            header('Content-Type: '.$http["content-type"]);
        }
        if(APIClass::T__IS($http, "allow"))
        {
            $t = $http["allow"];
            if(is_array($t))
            {
                $t = implode(", ", $t);
            }
            header("Allow: $t");
        }
        if(APIClass::T__IS($http, "cache"))
        {
            if(!is_array($http["cache"]))
            {
                header("Cache-Control: ".$http['cache']);
            }
            else
            {
                $i = 0;
                foreach($http["cache"] as $t)
                {
                    if($i == 0)
                    {
                        header("Cache-Control: ".$t);
                        $i++;
                        continue;
                    }
                    header("Cache-Control: ".$t, false);
                }
            }
        }
        if(APIClass::T__IS($http, "pragma"))
        {
            header("Pragma: ".$http["pragma"]);
        }
        if(APIClass::T__IS($http, "content"))
        {
            if(is_array($http["content"]))
            {
                foreach($http["content"] as $k => $v)
                {
                    switch($k)
                    {
                        case "disposition":
                            header("Content-Disposition: $v");
                            break;
                        case "encoding":
                            header("Content-Encoding: $v");
                            break;
                        case "language":
                            header("Content-Language: $v");
                            break;
                        case "length":
                            header("Content-Length: $v");
                            break;
                        case "location":
                            header("Content-Location: $v");
                            break;
                        case "md5":
                            header("Content-MD5: $v");
                            break;
                        case "range":
                            header("Content-Range: $v");
                            break;
                        case "type":
                            header("Content-Type: $v");
                            break;
                    }
                }
            }
        }
        if(APIClass::T__IS($http, "expires"))
        {
            header("Expires: ".$http["expires"]);
        }
        if(APIClass::T__IS($http, "last-modified"))
        {
            header("Last-Modified: ".$http["last-modified"]);
        }
        if(APIClass::T__IS($http, "location"))
        {
            header("Location: ".$http["location"]);
        }
        if(APIClass::T__IS($http, "warning"))
        {
            header("Warning: ".$http["warning"]);
        }
        if(APIClass::T__IS($http, "refresh"))
        {
            header("Refresh: ".$http["refresh"]);
        }
        if(APIClass::T__IS($http, "status"))
        {
            if(APIClass::T__IS($http["status"], "code") && APIClass::T__IS($http["status"], "msg"))
            {
                header("Status: ".$http["status"]["code"]." ".$http["status"]["msg"], true, $http["status"]["code"]);
                http_response_code($http["status"]["code"]);
                if(in_array($http["status"]["code"], APIClass::__sc_http_codes()))
                {
                    header($_SERVER["SERVER_PROTOCOL"]." ".$http["status"]["code"]." ".$http["status"]["msg"]);
                }
            }
        }
        // CORS
        header("Access-Control-Allow-Origin: *");
        print($data);
        return true;
    }
    public static function Request()
    {
        $request = [];
        $request["server"]["name"] = APIClass::T__get($_SERVER,"SERVER_NAME");
        $request["server"]["ip"] = APIClass::T__get($_SERVER,"SERVER_ADDR");
        $request["server"]["port"] = APIClass::T__get($_SERVER,"SERVER_PORT");
        $request["server"]["admin"] = APIClass::T__get($_SERVER,"SERVER_ADMIN");
        $request["server"]["protocol"] = APIClass::T__get($_SERVER,"SERVER_PROTOCOL");
        $request["server"]["software"] = APIClass::T__get($_SERVER,"SERVER_SOFTWARE");
        $request["method"] = APIClass::T__get($_SERVER,"REQUEST_METHOD");
        $request["client"]["ip"] = APIClass::T__get($_SERVER, "REMOTE_ADDR");
        $request["client"]["port"] = APIClass::T__get($_SERVER, "REMOTE_PORT");
        $request["request"]["time"] = APIClass::T__get($_SERVER, "REQUEST_TIME_FLOAT");
        $request["request"]["uri"] = APIClass::T__get($_SERVER, "REQUEST_URI");
        $request["http"]["protocol"] = APIClass::T__get($_SERVER,"SERVER_PROTOCOL");
        $t[0] = preg_match("/HTTP\/(?P<version>[\S]*)/", $request["http"]["protocol"], $t[1]);
        $request["http"]["version"] = ($t[0] == 1) ? $t[1]["version"] : null;
        $request["http"]["https"] = (APIClass::T__get($_SERVER, "HTTPS") == "on") ? true : false;
        $request["http"]["ssl"]["protocol"] = APIClass::T__get($_SERVER,"SSL_PROTOCOL");
        $request["http"]["ssl"]["cipher"]["cipher"] = APIClass::T__get($_SERVER,"SSL_CIPHER");
        $request["http"]["ssl"]["cipher"]["usekeysize"] = APIClass::T__get($_SERVER, "SSL_CIPHER_USEKEYSIZE");
        $request["http"]["ssl"]["cipher"]["algkeysize"] = APIClass::T__get($_SERVER, "SSL_CIPHER_ALGKEYSIZE");
        $request["http"]["accept"]["type"] = APIClass::T__get($_SERVER, "HTTP_ACCEPT");
        $request["http"]["accept"]["encoding"] = APIClass::T__get($_SERVER, "HTTP_ACCEPT_ENCODING");
        $request["http"]["connection"] = APIClass::T__get($_SERVER, "HTTP_CONNECTION");
        $request["http"]["host"] = APIClass::T__get($_SERVER, "HTTP_HOST");
        $request["http"]["user-agent"] = APIClass::T__get($_SERVER, "HTTP_USER_AGENT");
        $request["http"]["cache"] = APIClass::T__get($_SERVER, "HTTP_CACHE_CONTROL");
        $request["special"]["php_self"] = APIClass::T__get($_SERVER, "PHP_SELF");
        $request["special"]["document_root"] = APIClass::T__get($_SERVER, "DOCUMENT_ROOT");
        $request["special"]["request_uri"] = APIClass::T__get($_SERVER, "REQUEST_URI");
        $request["special"]["script_filename"] = APIClass::T__get($_SERVER, "SCRIPT_FILENAME");
        $request["special"]["script_uri"] = APIClass::T__get($_SERVER, "SCRIPT_URI");
        $request["special"]["script_url"] = APIClass::T__get($_SERVER, "SCRIPT_URL");
        $request["special"]["script_name"] = APIClass::T__get($_SERVER, "SCRIPT_NAME");
        $request["special"]["x-lscache"] = (APIClass::T__get($_SERVER, "X-LSCACHE") == "on") ? true : false;
        $t = $_REQUEST;
        unset($t["CMD"]);
        $request["data"] = $t;
        return $request;
    }
}