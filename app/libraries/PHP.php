<?php
/**
 * PHP Class
 * This class is for interacting with some PHP specific settings.
 * This class provides things like dieByVersion which will make script die if version is below specified level.
 * @author Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

/**
 * PHP Settings and Information Class
 */
class PHP
{
    /**
     * Checking What Version of PHP is on the server and returns it as array
     * Notation version()[0] . version()[1] . version()[2] (f.e. PHP 7.2.2 will be [7,2,2] as array
     * @return array Version of PHP on the Server
     */
    public static function version()
    {
        // PHP Version in String
        $version = phpversion();
        // Array of PHP Version Number but still as strings
        $version2 = explode(".", $version);
        $version = [];
        // We want integers to compare and other things
        foreach($version2 as $v)
        {
            $version[] = (int) $v;
        }
        return $version;
    }

    /**
     * Die if Version is to low
     * @param string $needed_version Needed Version to not die
     * @return bool True if PHP Version is good False if didn't die
     */
    public static function dieByVersion($needed_version)
    {
        // Actual Version
        $actual_version = phpversion();
        // Compare Versions
        if(version_compare($actual_version, $needed_version) >= 0)
        {
            // Continue if version is good
            if((VERBOSE !== null) && VERBOSE > 3)
            {
                echo("Your PHP Version: $actual_version");
            }
            if((VERBOSE !== null) && VERBOSE >= 255)
            {
                var_dump(phpinfo());
            }
            return true;
        }
        else
        {
            // Die if Version is too low
            die("Error: PHP Version to low!");
            die();
            return false;
        }
    }
    /**
     * Change very stupid settings which SHOULD BE default but aren't as default.
     * #StupidPHP
     */
    public static function repairPHP()
    {
        assert_options(ASSERT_BAIL, 1);
        #assert_options(ASSERT_CALLBACK, function() {
        #    die("Assertion Error");
        #});
    }
}
?>
