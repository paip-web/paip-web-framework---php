<?php
/**
 * Base Controller Class
 * This loads the models and views
 * @author Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

/**
 * Base Controller Class
 */
class Controller
{
    // Load Model
    public function model($model)
    {
        // Require Model File
        require_once '../app/models/'.$model.'.php';
        // Instansiate model
        return new $model();
    }
    // Load View
    public function view($view, $data=[])
    {
        // Check for the view file
        if(file_exists("../app/views/".$view.".php"))
        {
            require_once '../app/views/'.$view.'.php';
        }
        else
        {
            die("View Doesn't exist");
        }
    }
}