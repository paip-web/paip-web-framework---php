<?php
/**
 * App Core Class
 * Creates URL and loads core controller
 * URL: /controller/method/params
 * @author Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

/**
 * App Core Class
 */
class Core
{
    /**
     * Current Controller [Default Controller is Pages]
     */
    protected $currentController = "Pages";
    /**
     * Current Method [Default Method is index]
     */
    protected $currentMethod = "index";
    /**
     * Parameters for Controller Method
     */
    protected $params = [];
    /**
     * Application Core Class Constructor
     */
    public function __construct()
    {
        $url = $this->getUrl();
        // Look in controllers for first value
        if(file_exists('../app/controllers/'.ucwords($url[0]).'.php'))
        {
            // If exist, set as controller
            $this->currentController = ucwords($url[0]);
            // Unset 0 index
            unset($url[0]);
        }
        // Require Controller
        require_once '../app/controllers/'.$this->currentController.'.php';
        // Instansiate Controller
        $this->currentController = new $this->currentController;
        // Check for second part of url
        if(isset($url[1]))
        {
            // Check to see if method exist
            if(method_exists($this->currentController, $url[1]))
            {
                $this->currentMethod = $url[1];
                unset($url[1]);
            }
        }
        // Get Params
        $this->params = $url ? array_values($url) : [];
        // Call a callback if array of params
        call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
    }
    /**
     * Gets URL of the site wanted
     */
    public function getUrl()
    {
        if(isset($_GET["url"]))
        {
            $url = rtrim($_GET["url"], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode("/", $url);
            return $url;
        }
    }
}