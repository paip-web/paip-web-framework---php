<?php
/**
 * Database Class
 * Connect to Database
 * Create prepared statements
 * bind values
 * return rows and results
 *
 * @author Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

class Database
{
    /**
     * Database Host
     * Default: Config Value
     */
    private $host = DB_HOST;
    /**
     * Database User
     * Default: Config Value
     */
    private $user = DB_USER;
    /**
     * Database Password
     * Default: Config Value
     */
    private $pass = DB_PASS;
    /**
     * Database Name
     * Default: Config Value
     */
    private $dbname = DB_NAME;
    /**
     * Database Driver Handler
     */
    private $dbh;
    /**
     * Current Statement Handler
     */
    public $stmt;
    /**
     * Error
     */
    private $error;
    /**
     * Constructor of Database Class
     */
    public function __construct()
    {
        // Set DSN
        $dsn = "mysql:host=".$this->host.";dbname=".$this->dbname;
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => "set names 'utf8'"
        );
        // Catch PDO Exception
        try
        {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        }
        catch(PDOException $e)
        {
            $this->error = $e->getMessage();
            echo $this->error;
        }
    }
    /**
     * Prepare statement with query
     * @param string $sql SQL Query to Prepare Statement with
     */
    public function query($sql)
    {
        $this->stmt = $this->dbh->prepare($sql);
    }
    /**
     * Bind value
     * @param string $param Value Name to Bind Value
     * @param string $value Value to bind
     * @param int $type Type of Value
     */
    public function bind($param,$value, $type = null)
    {
        if(is_null($type))
        {
            switch(true)
            {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindParam($param, $value, $type);
    }
    /**
     * Execute prepared statement
     */
    public function execute()
    {
        return  $this->stmt->execute();
    }
    /**
     * Get result set as array of objects
     */
    public function resultSet()
    {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }
    /**
     * Get single record result as object
     */
    public function single()
    {
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }
    /**
     * Get count of rows from statement
     */
    public function rowCount()
    {
        return $this->stmt->rowCount();
    }
}