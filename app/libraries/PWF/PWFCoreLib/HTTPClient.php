<?php
/**
 * HTTP Client for PHP - PAiP Web Module
 * @author Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>
 * @copyright PAiP Web © 2018
 * @version 0.2.0dev0 
 */
/*
 * HTTP Client
 * 
 * @var string $HC_URL Request URL
 * @var string $HC_Method Request Method
 * @var string $HC_Body Request Body
 * @var string $HC_GET_PARAMS Request Get Parameters
 * @var string $HC_PARAMS Request Parameters
 */
class HTTPClient
{
    // Properties
    public $HC_URL;
    public $HC_Method;
    public $HC_Body;
    public $HC_GET_PARAMS;
    public $HC_PARAMS;
    public $HC_CURL_HTTPHEADER;
    public $HC_CURL_MAXREDIRECTS;
    public $HC_CURL_FOLLOWLOCATION;
    public $HC_CURL_COOKIEFILE;
    public $HC_CURL_USERAGENT;
    public $HC_CURL_REFERER;
    /**
     * Constructor
     */
    public function __construct()
    {
        // Created HTTP Client
        $this->HC_URL = "";
        $this->HC_Method = "GET";
        $this->HC_Body = "";
        $this->HC_GET_PARAMS = "";
        $this->HC_PARAMS = "";
        $this->HC_CURL_HTTPHEADER = array();
        $this->HC_CURL_MAXREDIRECTS = 4;
        $this->HC_CURL_FOLLOWLOCATION = true;
        $this->HC_CURL_COOKIEFILE = dirname(__FILE__).'/cookie.txt';
        $this->HC_CURL_USERAGENT = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1";
        $this->HC_CURL_REFERER = "http://www.google.com";
    }
    /**
     * Set Request Method
     * @param string $method Method of Request (f.e. "GET" or "POST")
     * @return object $this
     */
    public function method($method)
    {
        $this->HC_Method = $method;
        return $this;
    }
    /**
     * Set Request Method to GET
     * @return object $this
     */
    public function get()
    {
        return $this->method("GET");
    }
    /**
     * Set Request Method to POST
     * @return object $this
     */
    public function post()
    {
        return $this->method("POST");
    }
    /**
     * Set Request Url
     * @param string $uri URL for the Request
     * @return object $this
     */
    public function url($uri)
    {
        $this->HC_URL = $uri;
        return $this;
    }
    /**
     * Directly Set Request Parameters
     * @param string $params Parameters
     * @return object $this
     */
    public function setParams($params)
    {
        $this->HC_PARAMS = $params;
        return $this;
    }
    /**
     * Build Request Parameters from array
     * @param array $params Parameters Array
     * @return object $this
     */
    public function buildParams($params)
    {
        $this->setParams(http_build_query($params));
        return $this;
    }
    /**
     * Directly Set Request Parameters (GET Parameters)
     * Can be specified endpoint of the api (if url is set to root of api) by concatenating this and url together to make url
     * @param string $params Parameters
     * @return object $this
     */
    public function setGetParams($params)
    {
        $this->HC_GET_PARAMS = $params;
        return $this;
    }
    /**
     * Build Request Parameters from array (Get Parameters)
     * @param array $params Parameters Array
     * @return object $this
     */
    public function buildGetParams($params)
    {
        $this->setGetParams('?'.implode("&",$params));
        return $this;
    }
    /**
     * Build HTTP Response according to Variables that can be get from curl object
     * @param object $curl Curl Instance
     * @param object $response Response Text (Result of curl_exec()
     * @return object HttpResponse Object with all variables set
     */
    private function _curlLastRequestToHTTPResponse($curl, $response)
    {
        $responseobj = new HTTPResponse($response, curl_getinfo($curl, CURLINFO_HTTP_CODE));
        $responseobj->HR_CURL__EFFECTIVE_URL = curl_getinfo(
            $curl,
            CURLINFO_EFFECTIVE_URL
        );
        $responseobj->HR_STATUSCODE = curl_getinfo(
            $curl,
            CURLINFO_RESPONSE_CODE
        );
        $responseobj->HR_CURL__FILETIME = curl_getinfo(
            $curl,
            CURLINFO_FILETIME
        );
        $responseobj->HR_CURL__TOTAL_TIME = curl_getinfo(
            $curl,
            CURLINFO_TOTAL_TIME
        );
        $responseobj->HR_CURL__NAMELOOKUP_TIME = curl_getinfo(
            $curl,
            CURLINFO_NAMELOOKUP_TIME
        );
        $responseobj->HR_CURL__CONNECT_TIME = curl_getinfo(
            $curl,
            CURLINFO_CONNECT_TIME
        );
        $responseobj->HR_CURL__PRETRANSFER_TIME = curl_getinfo(
            $curl,
            CURLINFO_PRETRANSFER_TIME
        );
        $responseobj->HR_CURL__STARTTRANSFER_TIME = curl_getinfo(
            $curl,
            CURLINFO_STARTTRANSFER_TIME
        );
        $responseobj->HR_REDIRECT_COUNT = curl_getinfo(
            $curl,
            CURLINFO_REDIRECT_COUNT
        );
        $responseobj->HR_CURL__REDIRECT_TIME = curl_getinfo(
            $curl,
            CURLINFO_REDIRECT_TIME
        );
        $responseobj->HR_CURL__REDIRECT_URL = curl_getinfo(
            $curl,
            CURLINFO_REDIRECT_URL
        );
        $responseobj->HR_CURL__PRIMARY_IP = curl_getinfo(
            $curl,
            CURLINFO_PRIMARY_IP
        );
        $responseobj->HR_CURL__PRIMARY_PORT = curl_getinfo(
            $curl,
            CURLINFO_PRIMARY_PORT
        );
        $responseobj->HR_CURL__LOCAL_IP = curl_getinfo(
            $curl,
            CURLINFO_LOCAL_IP
        );
        $responseobj->HR_CURL__LOCAL_PORT = curl_getinfo(
            $curl,
            CURLINFO_LOCAL_PORT
        );
        $responseobj->HR_CURL__SIZE_UPLOAD = curl_getinfo(
            $curl,
            CURLINFO_SIZE_UPLOAD
        );
        $responseobj->HR_CURL__SIZE_DOWNLOAD = curl_getinfo(
            $curl,
            CURLINFO_SIZE_DOWNLOAD
        );
        $responseobj->HR_CURL__SPEED_DOWNLOAD = curl_getinfo(
            $curl,
            CURLINFO_SPEED_DOWNLOAD
        );
        $responseobj->HR_CURL__SPEED_UPLOAD = curl_getinfo(
            $curl,
            CURLINFO_SPEED_UPLOAD
        );
        $responseobj->HR_CURL__HEADER_SIZE = curl_getinfo(
            $curl,
            CURLINFO_HEADER_SIZE
        );
        $responseobj->HR_CURL__HEADER_OUT = curl_getinfo(
            $curl,
            CURLINFO_HEADER_OUT
        );
        $responseobj->HR_CURL__REQUEST_SIZE = curl_getinfo(
            $curl,
            CURLINFO_REQUEST_SIZE
        );
        $responseobj->HR_CURL__SSL_VERIFYRESULT = curl_getinfo(
            $curl,
            CURLINFO_SSL_VERIFYRESULT
        );
        $responseobj->HR_CURL__CONTENT_LENGTH_DOWNLOAD = curl_getinfo(
            $curl,
            CURLINFO_CONTENT_LENGTH_DOWNLOAD
        );
        $responseobj->HR_CURL__CONTENT_LENGTH_UPLOAD = curl_getinfo(
            $curl,
            CURLINFO_CONTENT_LENGTH_UPLOAD
        );
        $responseobj->HR_CONTENT_TYPE = curl_getinfo(
            $curl,
            CURLINFO_CONTENT_TYPE
        );
        $responseobj->HR_CURL__PRIVATE = curl_getinfo(
            $curl,
            CURLINFO_PRIVATE
        );
        $responseobj->HR_CURL__HTTP_CONNECTCODE = curl_getinfo(
            $curl,
            CURLINFO_HTTP_CONNECTCODE
        );
        $responseobj->HR_CURL__HTTPAUTH_AVAIL = curl_getinfo(
            $curl,
            CURLINFO_HTTPAUTH_AVAIL
        );
        $responseobj->HR_CURL__PROXYAUTH_AVAIL = curl_getinfo(
            $curl,
            CURLINFO_PROXYAUTH_AVAIL
        );
        $responseobj->HR_CURL__OS_ERRNO = curl_getinfo(
            $curl,
            CURLINFO_OS_ERRNO
        );
        $responseobj->HR_CURL__NUM_CONNECTS = curl_getinfo(
            $curl,
            CURLINFO_NUM_CONNECTS
        );
        $responseobj->HR_CURL__SSL_ENGINES = curl_getinfo(
            $curl,
            CURLINFO_SSL_ENGINES
        );
        $responseobj->HR_COOKIELIST = curl_getinfo(
            $curl,
            CURLINFO_COOKIELIST
        );
        $responseobj->HR_CURL__FTP_ENTRY_PATH = curl_getinfo(
            $curl,
            CURLINFO_FTP_ENTRY_PATH
        );
        $responseobj->HR_CURL__APPCONNECT_TIME = curl_getinfo(
            $curl,
            CURLINFO_APPCONNECT_TIME
        );
        $responseobj->HR_CERTINFO = curl_getinfo(
            $curl,
            CURLINFO_CERTINFO
        );
        $responseobj->HR_CURL__CONDITION_UNMET = curl_getinfo(
            $curl,
            CURLINFO_CONDITION_UNMET
        );
        $responseobj->HR_CURL__RTSP_CLIENT_CSEQ = curl_getinfo(
            $curl,
            CURLINFO_RTSP_CLIENT_CSEQ
        );
        $responseobj->HR_CURL__RTSP_CSEQ_RECV = curl_getinfo(
            $curl,
            CURLINFO_RTSP_CSEQ_RECV
        );
        $responseobj->HR_CURL__RTSP_SERVER_CSEQ = curl_getinfo(
            $curl,
            CURLINFO_RTSP_SERVER_CSEQ
        );
        $responseobj->HR_CURL__RTSP_SESSION_ID = curl_getinfo(
            $curl,
            CURLINFO_RTSP_SESSION_ID
        );
        $responseobj->HR_SERVER = $responseobj->HR_CURL__PRIMARY_IP.":".$responseobj->HR_CURL__PRIMARY_PORT;
        $responseobj->HR_CLIENT = $responseobj->HR_CURL__LOCAL_IP.":".$responseobj->HR_CURL__LOCAL_PORT;
        $responseobj->HR_ERROR = curl_error($curl);
        return $responseobj;
    }
    /**
     * Send Prepared Request
     * @return HTTPResponse HTTP Response Object
     */
    public function execute()
    {
        $s = curl_init(); 

        curl_setopt(
            $s,
            CURLOPT_URL,
            $this->HC_URL.$this->HC_GET_PARAMS
        );
        curl_setopt(
            $s,
            CURLOPT_HTTPHEADER,
            $this->HC_CURL_HTTPHEADER
        ); 
        curl_setopt(
            $s,
            CURLOPT_TIMEOUT,
            30
        ); 
        curl_setopt(
            $s,
            CURLOPT_MAXREDIRS,
            $this->HC_CURL_MAXREDIRECTS
        ); 
        curl_setopt(
            $s,
            CURLOPT_RETURNTRANSFER,
            true
        ); 
        @curl_setopt(
            $s,
            CURLOPT_FOLLOWLOCATION,
            $this->HC_CURL_FOLLOWLOCATION
        ); 
        curl_setopt(
            $s,
            CURLOPT_COOKIEJAR,
            $this->HC_CURL_COOKIEFILE
        ); 
        curl_setopt(
            $s,
            CURLOPT_COOKIEFILE,
            $this->HC_CURL_COOKIEFILE
        ); 

        if($this->HC_Method == "POST") 
        { 
            curl_setopt($s,CURLOPT_POST,true); 
            curl_setopt($s,CURLOPT_POSTFIELDS,$this->HC_PARAMS); 

        } 

        /* 
        if($this->_includeHeader) 
        { 
                curl_setopt($s,CURLOPT_HEADER,true); 
        } 

        if($this->_noBody) 
        { 
            curl_setopt($s,CURLOPT_NOBODY,true); 
        } 
        if($this->_binary) 
        { 
            curl_setopt($s,CURLOPT_BINARYTRANSFER,true); 
        } 
        */ 
        curl_setopt(
            $s,
            CURLOPT_USERAGENT,
            $this->HC_CURL_USERAGENT
        ); 
        curl_setopt(
            $s,
            CURLOPT_REFERER,
            $this->HC_CURL_REFERER
        ); 

        $response = $this->_curlLastRequestToHTTPResponse($s, curl_exec($s));

        #$this->_webpage = curl_exec($s); 
        #$this->_status = curl_getinfo($s,CURLINFO_HTTP_CODE); 
        curl_close($s);
        return $response;
    }
}
/**
 * DEV: Notes
 * If you are asking why properties are in CONSTANT Notation (all uppercase)
 * It's for making development easier. You want something from curl in 90% of cases you will can use from HTTPResponse that notation:
 *      HTTPResponseObject->HR_CURL__<SOMETHING FROM CURL>
 * And that properties are not to be much used outside of this class. (For everything you will need is method. Properties are for Debugging or Special Cases)
 */
?>