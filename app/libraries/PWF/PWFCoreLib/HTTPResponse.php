<?php
/**
 * HTTP Client for PHP - PAiP Web Module
 * @author Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>
 * @copyright PAiP Web © 2018
 * @version 0.2.0dev0 
 */
/**
 * HTTP Response
 * 
 * @var mixed $HR_STATUSCODE The last response code
 * @var mixed $HR_SERVER Server Side IP:PORT
 * @var mixed $HR_CLIENT Client Side IP:PORT
 * @var mixed $HR_REDIRECT_COUNT Number of redirects, with the CURLOPT_FOLLOWLOCATION option enabled
 * @var mixed $HR_CONTENT_TYPE Content-Type: of the requested document. NULL indicates server did not send valid Content-Type: header
 * @var mixed $HR_COOKIELIST All known cookies
 * @var mixed $HR_CERTINFO TLS certificate chain
 * @var mixed $HR_RESPONSETEXT Text of Response
 * @var mixed $HR_CURL__EFFECTIVE_URL Last effective URL
 * @var mixed $HR_CURL__FILETIME Remote time of the retrieved document, with the CURLOPT_FILETIME enabled; if -1 is returned the time of the document is unknown
 * @var mixed $HR_CURL__TOTAL_TIME Total transaction time in seconds for last transfer
 * @var mixed $HR_CURL__NAMELOOKUP_TIME Time in seconds until name resolving was complete
 * @var mixed $HR_CURL__CONNECT_TIME Time in seconds it took to establish the connection
 * @var mixed $HR_CURL__PRETRANSFER_TIME Time in seconds from start until just before file transfer begins
 * @var mixed $HR_CURL__STARTTRANSFER_TIME Time in seconds until the first byte is about to be transferred
 * @var mixed $HR_CURL__REDIRECT_TIME Time in seconds of all redirection steps before final transaction was started, with the CURLOPT_FOLLOWLOCATION option enabled
 * @var mixed $HR_CURL__REDIRECT_URL With the CURLOPT_FOLLOWLOCATION option disabled: redirect URL found in the last transaction, that should be requested manually next. With the CURLOPT_FOLLOWLOCATION option enabled: this is empty. The redirect URL in this case is available in CURLINFO_EFFECTIVE_URL
 * @var mixed $HR_CURL__PRIMARY_IP IP address of the most recent connection
 * @var mixed $HR_CURL__PRIMARY_PORT Destination port of the most recent connection
 * @var mixed $HR_CURL__LOCAL_IP Local (source) IP address of the most recent connection
 * @var mixed $HR_CURL__LOCAL_PORT Local (source) port of the most recent connection
 * @var mixed $HR_CURL__SIZE_UPLOAD Total number of bytes uploaded
 * @var mixed $HR_CURL__SIZE_DOWNLOAD Total number of bytes downloaded
 * @var mixed $HR_CURL__SPEED_UPLOAD Average download speed
 * @var mixed $HR_CURL__SPEED_DOWNLOAD Average upload speed
 * @var mixed $HR_CURL__HEADER_SIZE Total size of all headers received
 * @var mixed $HR_CURL__HEADER_OUT The request string sent. For this to work, add the CURLINFO_HEADER_OUT option to the handle by calling curl_setopt()
 * @var mixed $HR_CURL__REQUEST_SIZE Total size of issued requests, currently only for HTTP requests
 * @var mixed $HR_CURL__SSL_VERIFYRESULT Result of SSL certification verification requested by setting CURLOPT_SSL_VERIFYPEER
 * @var mixed $HR_CURL__CONTENT_LENGTH_DOWNLOAD Content length of download, read from Content-Length: field
 * @var mixed $HR_CURL__CONTENT_LENGTH_UPLOAD Specified size of upload
 * @var mixed $HR_CURL__PRIVATE Private data associated with this cURL handle, previously set with the CURLOPT_PRIVATE option of curl_setopt()
 * @var mixed $HR_CURL__HTTP_CONNECTCODE The CONNECT response code
 * @var mixed $HR_CURL__HTTPAUTH_AVAIL Bitmask indicating the authentication method(s) available according to the previous response
 * @var mixed $HR_CURL__PROXYAUTH_AVAIL Bitmask indicating the proxy authentication method(s) available according to the previous response
 * @var mixed $HR_CURL__OS_ERRNO Errno from a connect failure. The number is OS and system specific.
 * @var mixed $HR_CURL__NUM_CONNECTS Number of connections curl had to create to achieve the previous transfer
 * @var mixed $HR_CURL__SSL_ENGINES OpenSSL crypto-engines supported
 * @var mixed $HR_CURL__FTP_ENTRY_PATH Entry path in FTP server
 * @var mixed $HR_CURL__APPCONNECT_TIME Time in seconds it took from the start until the SSL/SSH connect/handshake to the remote host was completed
 * @var mixed $HR_CURL__CONDITION_UNMET Info on unmet time conditional
 * @var mixed $HR_CURL__RTSP_CLIENT_CSEQ Next RTSP client CSeq
 * @var mixed $HR_CURL__RTSP_CSEQ_RECV Recently received CSeq
 * @var mixed $HR_CURL__RTSP_SERVER_CSEQ Next RTSP server CSeq
 * @var mixed $HR_CURL__SESSION_ID RTSP session ID
 * @var mixed $HR_ERROR Error given from curl
 */
class HTTPResponse
{
    public $HR_STATUSCODE;
    public $HR_SERVER;
    public $HR_CLIENT;
    public $HR_REDIRECT_COUNT;
    public $HR_CONTENT_TYPE;
    public $HR_COOKIELIST;
    public $HR_CERTINFO;
    public $HR_RESPONSETEXT;
    public $HR_ERROR;
    public $HR_CURL__EFFECTIVE_URL;
    public $HR_CURL__FILETIME;
    public $HR_CURL__TOTAL_TIME;
    public $HR_CURL__NAMELOOKUP_TIME;
    public $HR_CURL__CONNECT_TIME;
    public $HR_CURL__PRETRANSFER_TIME;
    public $HR_CURL__STARTTRANSFER_TIME;
    public $HR_CURL__REDIRECT_TIME;
    public $HR_CURL__REDIRECT_URL;
    public $HR_CURL__PRIMARY_IP;
    public $HR_CURL__PRIMARY_PORT;
    public $HR_CURL__LOCAL_IP;
    public $HR_CURL__LOCAL_PORT;
    public $HR_CURL__SIZE_UPLOAD;
    public $HR_CURL__SIZE_DOWNLOAD;
    public $HR_CURL__SPEED_UPLOAD;
    public $HR_CURL__SPEED_DOWNLOAD;
    public $HR_CURL__HEADER_SIZE;
    public $HR_CURL__HEADER_OUT;
    public $HR_CURL__REQUEST_SIZE;
    public $HR_CURL__SSL_VERIFYRESULT;
    public $HR_CURL__CONTENT_LENGTH_DOWNLOAD;
    public $HR_CURL__CONTENT_LENGTH_UPLOAD;
    public $HR_CURL__PRIVATE;
    public $HR_CURL__HTTP_CONNECTCODE;
    public $HR_CURL__HTTPAUTH_AVAIL;
    public $HR_CURL__PROXYAUTH_AVAIL;
    public $HR_CURL__OS_ERRNO;
    public $HR_CURL__NUM_CONNECTS;
    public $HR_CURL__SSL_ENGINES;
    public $HR_CURL__FTP_ENTRY_PATH;
    public $HR_CURL__APPCONNECT_TIME;
    public $HR_CURL__CONDITION_UNMET;
    public $HR_CURL__RTSP_CLIENT_CSEQ;
    public $HR_CURL__RTSP_CSEQ_RECV;
    public $HR_CURL__RTSP_SERVER_CSEQ;
    public $HR_CURL__SESSION_ID;
    /**
     * Constructor of the HTTP Response
     * @param string $responseText Text of Response
     * @param string $statuscode Status Code of Response [Default=200]
     */
    public function __construct($responseText, $statuscode=200)
    {
        $this->HR_RESPONSETEXT = $responseText;
        $this->HR_STATUSCODE = $statuscode;
    }
    /**
     * Get Status Code of Request
     * @return int Status Code of Request
     */
    public function getStatusCode()
    {
        return $this->HR_STATUSCODE;
    }
    /**
     * Get Server Address
     * @return string Server Address in <ip>:<port> Notation
     */
    public function getServerAddress()
    {
        return $this->HR_SERVER;
    }
    /**
     * Get Client Address
     * @return string Client Address in <ip>:<port> Notation
     */
    public function getClientAddress()
    {
        return $this->HR_CLIENT;
    }
    /**
     * Get Redirect Count
     * @return int How much it redirected you to final url
     */
    public function getRedirectsCount()
    {
        return $this->HR_REDIRECT_COUNT;
    }
    /**
     * Get Content Type
     * @return string Content-Type: of Response
     */
    public function getContentType()
    {
        return $this->HR_CONTENT_TYPE;
    }
    /**
     * Get Cookies List
     * @return array List of Cookies
     */
    public function getCookies()
    {
        return $this->HR_COOKIELIST;
    }
    /**
     * Get Certificate Information
     * @return array Certificate Information
     */
    public function getCertificateInfo()
    {
        return $this->HR_CERTINFO;
    }
    /**
     * Get Response Text
     * @return string Text of Response
     */
    public function getResponse()
    {
        return $this->HR_RESPONSETEXT;
    }
    /**
     * Get Curl Error
     * @return string Error Code
     */
    public function getError()
    {
        return $this->HR_ERROR;
    }
    /**
     * Get Response As JSON Object
     * @return object json_decoded Response Text
     */
    public function getResponseAsJSON()
    {
        return json_decode($this->getResponse());
    }
}
?>