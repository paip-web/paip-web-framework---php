<?php
/**
 * Controller Interface
 * @author Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

namespace PWF\Interfaces;

/**
 * This is Interface for creating Controller
 */
interface ControllerInterface
{
    /**
     * Get Model
     * @param string $modelName Model Name
     * @return object Instanciated Model
     */
    public function model($modelName);
    /**
     * Get View
     * @param string $viewName Name of View
     * @return string View
     */
    public function view($viewName);
    /**
     * Get Partial View
     * @param string $viewName Name of Partial View
     * @return string Partial View
     */
    public function partial($viewName);
    /**
     * Get Request Data
     * @return array|object Request Data Object
     */
    public function request();
    /**
     * Render view
     * @param string $view View to render
     */
    public function render($view);
    /**
     * Response
     * @return object Response Object
     */
    public function response();
}