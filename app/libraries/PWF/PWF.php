<?php
/**
 * PAiP Web Framework Information Class
 * @author Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */
namespace PWF;
/**
 * PAiP Web Framework Information Class
 */
class PWF
{
    /**
     * Version Array Constant
     */
    const __VERSION_ARRAY__ = [
        "major" => 0,
        "minor" => 0,
        "patch" => 3,
        "level" => "dev",
        "level_release" => 0
    ];
    /**
     * Version String Constant
     */
    const __VERSION__ = PWF::__VERSION_ARRAY__["major"].".".PWF::__VERSION_ARRAY__["minor"].".".PWF::__VERSION_ARRAY__["patch"].PWF::__VERSION_ARRAY__["level"].PWF::__VERSION_ARRAY__["level_release"];
    /**
     * PWF Name Constant
     */
    const __NAME__ = "PAiP Web Framework";
    /**
     * PWF Full Name with version constant
     */
    const __FULL_NAME__ = PWF::__NAME__." v.".PWF::__VERSION__;
    /**
     * Get Version of PWF
     */
    public static function getVersion()
    {
        return PWF::__VERSION__;
    }
    /**
     * Get Version Array of PWF
     */
    public static function getVersionArray()
    {
        return PWF::__VERSION_ARRAY__;
    }
}