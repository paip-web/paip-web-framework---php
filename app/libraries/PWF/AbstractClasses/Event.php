<?php
/**
 * Event Interface
 * @author Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

namespace PWF\AbstractClasses;

/**
 * This is base class of Event for use with Event Manager.
 */
abstract class Event
{
    /**
     * Event Listener Name this event is waiting for
     */
    public $eventlistenername;
    /**
     * Call if context is $callif
     */
    public $callif = null;
    /**
     * Run function
     * @param mixed $context Context
     * @param mixed $data Data for Event Run
     */
    public abstract function run($context, $data=[]);
    /**
     * This function runs before method run.
     * This function is for setting up everything needed for event call.
     */
    public abstract function setup();
    /**
     * This function runs after method run.
     * This function is for cleaning up everything after event call.
     */
    public abstract function finish();
}