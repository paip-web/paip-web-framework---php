<?php
/**
 * PAiP Web Framework Information Class
 * @author Patryk Adamczyk <patrykadamczyk@patrykadamczyk.net>
 * @copyright 2018 Patryk Adamczyk <patrykadamczyk@paipweb.com>
 * @copyright 2018 PAiP Web
 */

class PWF
{
    const __VERSION__ = "0.0.3dev0";
    const __NAME__ = "PWF";
    const __FULL_NAME__ = PWF::__NAME__." v.".PWF::__VERSION__;
}