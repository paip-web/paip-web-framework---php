<?php
/**
 * Icon to HTML
 * @param int $itype Type
 * @param string $iname Name
 * @param string $isrc SRC
 * @return null|string Printed String or NULL on ERROR
 */
function PW__ICON__TYPE2HTML($itype, $iname, $isrc)
{
    switch ($itype)
    {
        case 1:
            $print = '<link rel="'.$iname.'" href="'.$isrc.'"/>';
            echo($print);
            return $print;
        case 2:
            $print = '<meta name="'.$iname.'" content="'.$isrc.'"/>';
            echo($print);
            return $print;
        case 3:
            die("Error: Not Implemented Feature");
            return null;
        default:
            return null;
    }
}
?>
<!DOCTYPE html>
<html lang="<?= SITELANG ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= SITENAME ?></title>
    <?php if(isset($author)): ?>
    <meta name="author" content="<?= $author ?>"/>
    <meta name="web_author" content="<?= $author ?>"/>
    <?php endif; ?>
    <?php if(isset($contact)): ?>
    <meta name="contact" content="<?= $contact ?>"/>
    <?php endif; ?>
    <?php if(isset($copyright)): ?>
    <meta name="copyright" content="<?= $copyright ?>"/>
    <?php endif; ?>
    <?php if(isset($robots)): ?>
    <meta name="robots" content="<?= $robots ?>"/>
    <?php endif; ?>
    <?php if(isset($humanstxt)): ?>
    <link type="text/plain" rel="author" href="<?= URLROOT ?>/humans.txt"
    <?php endif; ?>
    <?php if(isset($description)): ?>
    <meta name="description" content="<?= $description ?>"/>
    <?php endif; ?>
    <?php if(isset($keywords)): ?>
    <meta name="keywords" content="<?= $keywords ?>"/>
    <?php endif; ?>
    <meta name="application-name" content="<?= SITENAME ?>"/>
    <?php if(isset($icons) && is_array($icons)): ?>
        <?php foreach($icons as $k => $v): ?>
            <?php
                // Icon Tags
                /**
                 * 0 - NULL
                 * 1 - <link rel="{iname}" href="{src}"/>
                 * 2 - <meta name="{iname}" href="{src}"/>
                 * 3 - PWF Trying to make from one file every another required file
                 */
                switch($k)
                {
                    // META KEYS - 1
                    case "icon":
                        PW__ICON__TYPE2HTML(1, "icon", $v);
                        PW__ICON__TYPE2HTML(1, "shortcut", $v);
                        break;
                    case "apple-touch-icon":
                        PW__ICON__TYPE2HTML(1, "apple-touch-icon", $v);
                        break;
                    case "msapplication-square70x70":
                        PW__ICON__TYPE2HTML(2, "msapplication-square70x70logo", $v);
                        break;
                    case "msapplication-square150x150":
                        PW__ICON__TYPE2HTML(2, "msapplication-square150x150logo", $v);
                        break;
                    case "msapplication-square310x150":
                        PW__ICON__TYPE2HTML(2, "msapplication-square310x150logo", $v);
                        break;
                    case "msapplication-square310x310":
                        PW__ICON__TYPE2HTML(2, "msapplication-square310x310logo", $v);
                        break;
                    case "msapplication-TileColor":
                        PW__ICON__TYPE2HTML(2, "msapplication-TileColor", $v);
                        break;
                    // PWF KEYS - 2
                    case "pwf-icon-ico":
                        PW__ICON__TYPE2HTML(1, "icon", $v);
                        PW__ICON__TYPE2HTML(1, "shortcut", $v);
                        break;
                    case "pwf-icon-180x180":
                        PW__ICON__TYPE2HTML(1, "apple-touch-icon", $v);
                        break;
                    case "pwf-icon-70x70":
                        PW__ICON__TYPE2HTML(2, "msapplication-square70x70logo", $v);
                        break;
                    case "pwf-icon-150x150":
                        PW__ICON__TYPE2HTML(2, "msapplication-square150x150logo", $v);
                        break;
                    case "pwf-icon-310x150":
                        PW__ICON__TYPE2HTML(2, "msapplication-square310x150logo", $v);
                        break;
                    case "pwf-icon-310x310":
                        PW__ICON__TYPE2HTML(2, "msapplication-square310x310logo", $v);
                        break;
                    case "pwf-icon-color":
                        PW__ICON__TYPE2HTML(2, "msapplication-TileColor", $v);
                        break;
                    // PWF AUTOMAKE
                    case "pwf-icon":
                        die("Error: Not Implemented Key");
                    default:
                        die("Error: Invalid Key");
                }
            ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <link type="text/css" rel="stylesheet" href="<?= URLROOT ?>/assets/app/main.css"/>
</head>
<body>
    