# Includes README
## Variables and other things for workflow with default header and footer
### Variables
```php
<?php
string $description; # <meta name="description"/>, if null tag is omitted
string $keywords; # <meta name="keywords"/>, if null tag is omitted
string $author; # <meta name="author"/>, if null tag is omitted
string $copyright; # <meta name="copyright"/>, if null tag is omitted
string $contact; # <meta name="contact"/>, if null tag is omitted
string $robots; # <meta name="robots"/>, if null tag is omitted
bool $humanstxt; # <link rel="author"/>, if null tag is omitted
array $icons; # <meta name="*"/>, if null tag is omitted
$icons = [
    // Meta Tag --> array()
    "icon" => "X",
    "apple-touch-icon" => "X",
    "msapplication-square70x70" => "X",
    "msapplication-square150x150" => "X",
    "msapplication-square310x150" => "X",
    "msapplication-square310x310" => "X",
    "msapplication-TileColor" => "#000000",
    // PWF Icon Names
    "pwf-icon-ico" => "X",
    "pwf-icon-180x180" => "X",
    "pwf-icon-70x70" => "X",
    "pwf-icon-150x150" => "X",
    "pwf-icon-310x150" => "X",
    "pwf-icon-310x310" => "X",
    "pwf-icon-color" => "X"
];
```
### Other
```markdown
These files
* public/assets/app/main.js
* public/assets/app/main.css
Should be concatenated and minified all JavaScript Scripts and CSS Stylesheets

I suggest using gulp or grunt to do this automatically.

```